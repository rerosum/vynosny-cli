Local Ubuntu Dev Setup:

    To get Python source, make, install:
        cd ~
        wget https://www.python.org/ftp/python/3.5.2/Python-3.5.2.tgz
        tar -xvzf Python-3.5.2.tgz
        cd Python-3.5.2
        ./configure
        make
        make test
        sudo make install
        sudo apt install python-pip

    Install doit task manager:
        sudo apt install python-doit

    Ubuntu and Parallels - Login loop issue:
        Unable to login after Parallels tools is installed
        http://kb.parallels.com/en/118776

    To update the virtual environment 'venv':
        sudo apt install virtualenv
        cd ~/DEV/projects/vynosny-py
        rm -rf ./venv/bin/python
        virtualenv -p /usr/bin/python3.5 venv
        ./venv/bin/pip install -r requirements.txt

    To update Node/NPM env with ReactJS

        Install Node/NPM
            curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -
            sudo apt-get install -y nodejs
            npm install -g create-react-app

    Install docker & compose:
            curl -fsSL https://get.docker.com/ | sh
            docker -v
            sudo -i
                curl -L https://github.com/docker/compose/releases/download/1.9.0-rc4/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
                sudo chmod +x /usr/local/bin/docker-compose
                exit

    Enable access to Docker private registry over HTTPS
        1. Create or modify /etc/docker/daemon.json
            { "insecure-registries":["192.168.1.2:5000"] }
        2. Restart docker daemon
            sudo service docker restart

    BitBucket SSH & Pushing code:
        1. Create new ssh key
        2. Paste ssh key into BitBucket

    Database GUI - Valentina Studio:
        http://www.valentina-db.com/en/download-valentina-studio/current/vstudio_x64_lin-deb?format=raw

Vynosny Host Server Setup:

    Install SSD drive in host machine:
        Review mounted file systems:
            df

        See what devices are powered on in the host:
            sudo lshw -C disk

        Partition the new drive (/dev/sdb):
            sudo fdisk /dev/sdb
                Create a new primary partition

        Format partition:
            sudo mkfs -t ext3 /dev/sdb1
            sudo tune2fs -m 1 /dev/sdb1
            sudo fdisk -l

        Create mount point:
            sudo mkdir /data
            sudo nano -Bw /etc/fstab
                Add to end-> /dev/sdb1    /data   ext3    defaults     0        2
            sudo mount -a
            df

    Backup to Google drive:
        cd ~ && wget https://docs.google.com/uc?id=0B3X9GlR6EmbnWksyTEtCM0VfaFE&export=download
        mv uc\?id\=0B3X9GlR6EmbnWksyTEtCM0VfaFE gdrive
        chmod +x gdrive
        sudo install gdrive /usr/local/bin/gdrive
        gdrive list
        gdrive upload --recursive /data

    Docker cheat sheet:
        https://github.com/wsargent/docker-cheat-sheet
        docker commit <CONTAINER> 192.168.1.2:5000

    Install docker & compose:
        curl -fsSL https://get.docker.com/ | sh
        docker -v
        sudo -i
            curl -L https://github.com/docker/compose/releases/download/1.9.0-rc4/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose
            sudo chmod +x /usr/local/bin/docker-compose
            exit

    Find stale IP tables & delete them when the container can't be reached from host(I think is is fixed in docker v.1.11.2):
        sudo iptables -L -t nat --line-numbers
        iptables -t nat -D DOCKER [line-number from above]
    
    Setup private docker registry:
        Good resource for setting up registry with HTTPS/nginx: https://www.digitalocean.com/community/tutorials/how-to-set-up-a-private-docker-registry-on-ubuntu-14-04

        In -> /etc/sysctl.conf
        Ensure -> net.ipv6.conf.all.forwarding=1
        Add for http push to repo:
            in -> /etc/docker/daemon.json
                { "insecure-registries":["192.168.1.2:5000"] }
        sudo service docker restart

        docker run -d -p 5000:5000 --restart=always --name registry -v `pwd`/data:/var/lib/registry registry

        Test registry:
            docker pull rerosum/mariadb && docker tag rerosum/mariadb 192.168.1.2:5000/mariadb
            docker push 192.168.1.2:5000/mariadb
