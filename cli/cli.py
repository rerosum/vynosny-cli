"""
v
 
Usage:
  v hello
  v -h | --help
  v --version
 
Options:
  -h --help                         Show this screen.
  --version                         Show version.
 
Examples:
  v hello
 
Help:
  For help using this tool, please open an issue on the Github repository:
  https://bitbucket.org/rerosum/vynosny-cli
"""
from inspect import getmembers, isclass
from docopt import docopt

VERSION = '1.0.0'
 
def main():
    """Main CLI entrypoint."""
    try:
      import commands
      options = docopt(__doc__, version=VERSION)
      # Here we'll try to dynamically match the command the user is trying to run
      # with a pre-defined command class we've already created.
      for k, v in options.items():
          if hasattr(commands, k) and v:
              module = getattr(commands, k)
              commands = getmembers(module, isclass)
              command = [command[1] for command in commands if command[0] != 'Base'][0]
              command = command(options)
              command.run()
    except Exception as e:
      print(str(e))

main()            