from doit.action import CmdAction
from doit.tools import LongRunning
from  app import settings
import os
import subprocess

DOIT_CONFIG = {'default_tasks': ['build', 'start']}

def task_react():
    return {
        'actions' : ['cd ./app/react && npm start'],
        'task_dep': []
    }

def task_build_react():
    return {
        'actions' : ['cd ./app/react && rm -rf static||true && npm run build && cp -a build/static . && cp -a build/*.* .'],
        'task_dep': []
    }

def task_py_compile():
    return {
        'actions' : ['python -m compileall -q ./app'],
        'task_dep' : []
    }

def task_build():
    return {
        'actions' : ['echo building...'],
        'task_dep': ['py_compile', 'create_compose', 'remove']
    }

def task_create_compose():
    yield {
        'name': 'create_compose',
        'actions': [
            LongRunning('docker-compose build')
        ],
        'task_dep': []
    }

def task_create():
    port = settings.SERVICE_PORT
    for serviceName in serviceList():
        f = './app/Dockerfile' if serviceName != 'webapp' else './app/DockerfileWebapp'
        yield {
            'name': serviceName,
            'actions': [
                LongRunning('rm -rf ./app/test/%s/__pycache__' % serviceName),
                LongRunning('docker build -f %s --build-arg SERVICE_NAME=%s --build-arg SERVICE_PORT=%s -t %s/%s:latest ./app' % (f, serviceName, port, settings.DOCKER_REGISTRY, serviceName))
            ],
            'task_dep': []
        }

def task_start():
    return {
        'actions': [LongRunning('docker-compose up -d')]
    }

def task_db():
    return {
        'actions': [LongRunning('docker-compose start -d db')]
    }

def task_stop():
    return {
        'actions': [LongRunning('docker-compose down')]
    }

def task_remove():
    return {
        'actions': [LongRunning('docker rm $( docker ps -q -f status=exited )')],
        'actions': [LongRunning('docker rmi $( docker images -q -f dangling=true )')]
    }

def task_remove_containers():
    return {
        'actions': [LongRunning('docker rm $(docker ps -a -q)')]
    }

def task_push():
    for serviceName in serviceList():
        yield {
            'name': 'push-'+serviceName,
            'actions': [
                LongRunning('docker login -u %s -p %s %s && docker push %s/%s' % (settings.DOCKER_USERNAME, settings.DOCKER_PASSWORD, settings.DOCKER_REGISTRY, settings.DOCKER_REGISTRY, serviceName))
            ],
            'task_dep': ['test']
        }

def task_wait():
    yield {
        'name': 'wait',
        'actions': [
            LongRunning('sleep 1')
        ],
        'task_dep': ['start']
    }

def task_test():

    def testService(serviceName):
        retVal = False
        try:
            out = subprocess.check_output(
                #'docker exec vynosnypy_%s_1 curl -s http://localhost:7777/test' % serviceName,
                'docker exec vynosnypy_%s_1 python testor.py' % serviceName,
                stderr=subprocess.STDOUT,
                shell=True
            )
            strOut = out.decode('utf-8')
            outLines = strOut.split('\n')
            rc = outLines[0]
            if rc != '0':
                for line in outLines[1:]:
                    print(line)
            retVal = True if rc == '0' else False
        except Exception as e:
            retVal = str(e)
        return retVal

    for serviceName in serviceList():
        yield {
            'name': serviceName,
            'actions': [
                (testService, [serviceName])
            ],
            'task_dep': ['start']
        }

def serviceList():
    services = []
    dir_path = os.path.dirname(os.path.realpath(__file__))
    services_path = os.path.join(dir_path, 'app', 'services')
    for serviceName in os.listdir(services_path):
        if os.path.isdir(os.path.join(services_path, serviceName)):
            if not serviceName.startswith('__'):
                services.append(serviceName.lower())
    return services

"""
    DEPRECATED TASKS
"""
def task_copy_static_files():
    return {
        'actions': [LongRunning('rm -r /home/bob/DEV/projects/vynosny-py/app/services/webapp/react'),
                    LongRunning('cp -a /home/bob/DEV/projects/vynosny-react/build/. /home/bob/DEV/projects/vynosny-py/app/services/webapp/react')]
    }

def task_req_install():
    return {
        'actions': [CmdAction('pip install -r requirements.txt')]
    }

def task_mariadb():
    return {
        'actions': ['docker run --net=host --name mariadb --restart=always -p 3606:3606 -e MYSQL_ROOT_PASSWORD=picompute -d rerosum/mariadb']
    }

def task_run_container():
    # docker run -d -i -p 5000:5000 --env SERVICE_NAME='mbtrading' --env SERVICE_PORT='5000' --name mbtrading rerosum/vynosny-ms
    return {
        'actions': [LongRunning('docker run --net=host -d -i -p ' + str(settings.SERVICE_PORT) + ':' + str(settings.SERVICE_PORT) +
                                ' --env SERVICE_NAME=' + 'NONE' +
                                ' --env DB_HOST=' + settings.DB_HOST +
                                ' --env DB_PORT=' + settings.DB_PORT +
                                ' --env DB_USER=' + settings.DB_USER +
                                ' --env DB_PASSWORD=' + settings.DB_PASSWORD +
                                ' --env DB_NAME=' + settings.DB_NAME +
                                ' --env SERVICE_PORT=' + str(settings.SERVICE_PORT) +
                                ' --name ' + 'NONE' + ' ' + settings.DOCKER_USERNAME + '/' + 'vynosny-ms')]
    }

def task_run_db_container():
    # docker run --name mariadbtest -e MYSQL_ROOT_PASSWORD=mypass -d mariadb
    return {
        'actions': [LongRunning('docker run --net=host -d -i ' +
                                ' --env MYSQL_ROOT_PASSWORD=' + settings.DB_PASSWORD +
                                ' --name mariadb ' + settings.DOCKER_REGISTRY + '/' + 'mariadb')]
    }
